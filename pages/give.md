---
title: Give
date: 2021-05-01T04:00:00Z
slug: give
status: true
author: admin
layout: page
---
<section class=\"mw7 center\">\n  <h2 class=\"ph3 ph0-l\">{{ title }}</h2>\n
  \ <div class=\"pv4 bt b--black-10 ph3 ph0-l flex flex-column\">\n<article class=\"cf
  bg-near-white \">\n  <div class=\"fl w-50 tc\">\n    <h1>service times</h1>\n    <p>sundays
  10:25am<br/> \n\t\twednesdays 7:00pm<br/>\n\t\t<br/>\n\t\t</p>\n  </div>\n  <div
  class=\"fl w-50 tc\">\n\t<h1>location</h1>\n\t\t<p>3339 wade hampton blvd<br/>\n\t\ttaylors,
  sc<br/>\n\t\t(864)244-0207<br/>\n\t\tinfo@myffc.org</p>\t\n  </div>\n</article>\n<article
  class=\"pt4 cf\">\n  <div class=\"pv4 bt b--black-10 ph3 flex flex-column tc\">\n
  \    <i>You will be enriched in every way so that you can be generous on every occasion,
  and through us your generosity will result in thanksgiving to God.</i>\n     <b>2
  Corinthians 9:11</b><br/>We believe that God loves a cheerful giver and at the heart
  of the Christian faith is generosity. We know our God is a generous giver and want
  to be more like Him. The Word of God calls us to be stewards of what we're given
  and to follow the principles of tithes and offerings as a way to establish His church
  and reach others with the gospel. We're never more like our God when we give, so
  we never hesitate to be generous.\n     </br/>\n\t <br/>\n\t <br/>\n\t <br/>\n     <div
  class=\"fl w-100 tc\">\n        <button class=\"tithely-give-btn\" style=\"background-color:
  #00DB72;font-family: inherit;font-weight: bold;font-size: 19px; padding: 15px 70px;
  border-radius: 4px; cursor: pointer; background-image: none; color: white; text-shadow:
  none; display: inline-block; float: none; border: none;\" data-church-id=\"261780\">Click
  to Give</button>\n        <script src=\"https://tithe.ly/widget/v3/give.js?3\"></script>\n
  \       <script>\n            var tw = create_tithely_widget();\n        </script>\n
  \       <br/>\n\t\t<h2>Thank you!</h2>\n\t</div>\n</article>\n</div>\n</section>"
